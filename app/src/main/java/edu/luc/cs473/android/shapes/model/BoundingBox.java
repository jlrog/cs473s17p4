package edu.luc.cs473.android.shapes.model;

import java.util.List;
import java.util.Arrays;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */

public class BoundingBox implements Visitor<Location> {

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {

        return f.getShape().accept(this);
    }

	@Override
	public Location onGroup(final Group g) {
        Integer minX = null;
        Integer minY = null;
        Integer maxX = null;
        Integer maxY = null;
        for (Shape shape : g.getShapes()) {
            Location boundingBox = shape.accept(this);
            Rectangle rectangle = (Rectangle) boundingBox.getShape();
            if (minX == null && maxX == null) {
                minX = boundingBox.getX();
                maxX = boundingBox.getX() + rectangle.getWidth();
            } else if (boundingBox.getX() < minX) {
                minX = boundingBox.getX();
            }
            if (maxX < boundingBox.getX() + rectangle.getWidth()) {
                maxX = boundingBox.getX() + rectangle.getWidth();
            }
            if (minY == null && maxY == null) {
                minY = boundingBox.getY();
                maxY = boundingBox.getY() + rectangle.getHeight();
            } else if (boundingBox.getY() < minY) {
                minY = boundingBox.getY();
            }
            if (maxY < boundingBox.getY() + rectangle.getHeight()) {
                maxY = boundingBox.getY() + rectangle.getHeight();
            }
        }

        return new Location(minX, minY, new Rectangle(maxX-minX, maxY-minY));
    }

	@Override
	public Location onLocation(final Location l) {
        Location boundingBox = l.getShape().accept(this);
        return new Location(l.getX()+boundingBox.getX(), l.getY()+boundingBox.getY(), boundingBox.getShape());
    }

	@Override
	public Location onRectangle(final Rectangle r) {
        return new Location(0, 0, r);
    }

	@Override
	public Location onStroke(final Stroke c) {
        return c.getShape().accept(this);
    }

	@Override
	public Location onOutline(final Outline o) {
        return o.getShape().accept(this);
    }

	@Override
	public Location onPolygon(final Polygon s) {
        List<? extends Point> points = s.getPoints();
        int [] x_axis =  new int[points.size()];
        int [] y_axis = new int[points.size()];
        int cnt = 0 ;
        for (Point axis : points) {
            x_axis[cnt]= axis.getX();
            y_axis[cnt]= axis.getY();
            cnt++;
        }
        Arrays.sort(x_axis);
        Arrays.sort(y_axis);
        final int width = x_axis[x_axis.length-1]-x_axis[0];
        final int height = y_axis[x_axis.length-1]-y_axis[0];
        return new Location(x_axis[0],y_axis[0],new Rectangle(width,height));
	}

}