package edu.luc.cs473.android.shapes.model;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */

public class Size implements Visitor<Integer> {

	// TODO entirely your job
	@Override
	public Integer onPolygon(final Polygon p) {
		Integer size = null;
		if (p.getPoints().size()>=3){
			size = 1;
		}
		return size;
	}

	@Override
	public Integer onCircle(final Circle c) {
		Integer size = null;
		if(c.getRadius() >=0){
			size = 1;
		}
		return size;
	}

	@Override
	public Integer onGroup(final Group g) {
		Integer size = 0;
		for(Shape shape:g.getShapes()){
			if(shape.accept(this)!=null){
				size+=shape.accept(this);
			}else{
				size+=onGroup((Group) shape);
			}
		}
		return size;
	}

	@Override
	public Integer onRectangle(final Rectangle q) {
		Integer size = null;
		if(q.getWidth()>=0&& q.getHeight()>=0){
			size = 1;
		}
		return size;
	}

	@Override
	public Integer onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Integer onFill(final Fill c) {
		return c.getShape().accept(this);
	}

	@Override
	public Integer onLocation(final Location l) {
		return l.getShape().accept(this);
	}

	@Override
	public Integer onStroke(final Stroke c) {
		return c.getShape().accept(this);
	}
}
