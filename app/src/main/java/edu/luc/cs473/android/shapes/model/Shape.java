package edu.luc.cs473.android.shapes.model;

/**
 * A graphical shape.
 */

public interface Shape {
	<Result> Result accept(Visitor<Result> v);
}
