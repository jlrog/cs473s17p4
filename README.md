# Design patterns 

On  this project we implemented a simple API for drawing graphical shapes on an Android application using the following design patterns.

 * Composite pattern
 * Decorator pattern
 * Visitor pattern

## Design Pattern Application

The Composite pattern was used to perform operations on both primitive objects and composites in same way, without differentiate between the two. We also implemented the 

Decorator pattern that allows to add new functionality to existing objects without altering the structure acting as a wrapper to existing class, by creating a decorator class which wraps the original class and provides additional functionality while keeping class methods intact.

Finally we implemented the Visitors pattern that allows to change the executing methods of an element class. Hence we can change the method behavior depending on the visitor of the class.

# Setting up the Environment

Check out the project using Android Studio. This creates the `local.properties` file
with the required line

    sdk.dir=< PATH_TO_ANDROID_STUDIO>

# Running the Application

In Android Studio: `Run > Run app`

# Running the Tests

## Unit tests

In Android Studio:

* `View > Tool Windows > Build Variants`
* `Test Artifact: Unit Tests`
* right-click on `app/java/edu...shapes (test)`, then choose `Run Tests in edu...`